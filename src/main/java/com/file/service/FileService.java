package com.file.service;

import com.file.model.File;
import com.file.repository.FileRepository;
import org.apache.commons.io.FilenameUtils;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class FileService
{
    @Autowired
    private FileRepository repo;

    //Method to Upload file
    public String fileUpload(String title,MultipartFile getfile)
    {
        File file = null;
        try
        {
             file = repo.findByTitle(title);

             if(file == null)
            {
                file = new File();
                file.setTitle(title);
                file.setFile(new Binary(BsonBinarySubType.BINARY,getfile.getBytes()));

                String extension = "."+FilenameUtils.getExtension(getfile.getOriginalFilename());
                file.setExtension(extension);

                file = repo.insert(file);
                return "File created";
            }
        }
        catch (Exception e)
        {
           e.printStackTrace();
        }
        return "File already exists";
    }

    //Method to retrieve file
    public String fileRetrieve(String fileName)
    {
        File data = null;
        try
        {
             data = repo.findByTitle(fileName);
             if(data == null)
             {
                 return "File not available";
             }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String file = data.getTitle() + data.getExtension();
        MultipartFileConvertor multipartFileConvertor = new MultipartFileConvertor(data.getFile().getData(), file);

        try
        {
            multipartFileConvertor.transferTo(multipartFileConvertor.getFile());
            multipartFileConvertor.clearOutStreams();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "File retrieved in path: "+multipartFileConvertor.getFilePath();
    }

    //Method to delete all files
     public String deleteAll()
     {
         repo.deleteAll();
         return "Deleted All Files";
     }
}


















