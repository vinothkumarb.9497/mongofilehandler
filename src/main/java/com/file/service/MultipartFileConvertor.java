package com.file.service;

import lombok.Data;

import java.io.*;

@Data
public class MultipartFileConvertor
{

    private final byte[] fileContent;

    private File file = null;

    private String fileName;

    private String localPath = System.getProperty("java.io.tmpdir");

    private String filePath;

    private FileOutputStream fileOutputStream;

    public MultipartFileConvertor(byte[] fileData, String name)
    {
        this.fileContent = fileData;
        this.fileName = name;
        this.filePath = localPath + name;
        file = new File(filePath);
    }

    public void transferTo(File dest) throws IOException, IllegalStateException
    {
        fileOutputStream = new FileOutputStream(dest);
        fileOutputStream.write(fileContent);
    }

    public void clearOutStreams() throws IOException
    {
        if (null != fileOutputStream)
        {
            fileOutputStream.flush();
            fileOutputStream.close();
            file.deleteOnExit(); //Deletes file when program stops
        }
    }
}
