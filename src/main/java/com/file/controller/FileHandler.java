package com.file.controller;

import com.file.service.FileService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;


@RestController
public class FileHandler
{
    @Autowired
    FileService service;

    @PostMapping("uploadFile")
    public String upload(
            @ApiParam(name = "File", value = "File", type = "File", required = true,example = "filname.extension")
            @RequestPart("File") MultipartFile file,
            @ApiParam(name = "Title", value = "Title", type = "String", required = true, example = "title")
            @RequestParam("Title") String title
            ) throws IOException
    {
        return service.fileUpload(title,file);
    }

    @GetMapping("retrieveFile")
    public String retrieve(
            @ApiParam(name = "File Name", value = "File Name", type = "String", required = true ,example = "File name")
            @RequestParam("File Name") String fileName
    ) throws IOException
    {
        return service.fileRetrieve(fileName);
    }

    @DeleteMapping("DeleteAll")
    public String deleteAll()
    {
        return service.deleteAll();
    }
}
