package com.file.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "files")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class File implements Serializable {
    @Id
    private String id;
    private String title;
    private String extension;
    private Binary file;
}
