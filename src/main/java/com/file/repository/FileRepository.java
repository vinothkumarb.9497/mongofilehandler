package com.file.repository;

import com.file.model.File;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends MongoRepository<File,String>
{
    @Query("{'title' : ?0}")
    public File findByTitle(String title);
}
